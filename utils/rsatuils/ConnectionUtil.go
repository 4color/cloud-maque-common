package rsatuils

import (
	"log"
	"strings"
)

/*
*
数据库加密
*/
func ConnectionEncode(ConnectionString string) string {
	if ConnectionString == "" {
		return ConnectionString
	}
	str, err2 := RsaEncode(ConnectionString)
	if err2 != nil {
		log.Print("数据库连接加密失败：" + err2.Error())
		return ConnectionString
	}
	ConnectionString = str

	return ConnectionString
}

/*
*
数据库解密
*/
func ConnectionDecode(ConnectionString string) string {

	if ConnectionString == "" {
		return ConnectionString
	}

	tmp := strings.ToLower(ConnectionString)
	if !(strings.Contains(tmp, "user") || strings.Contains(tmp, "jdbc") || len(tmp) < 20) {
		str, err2 := RsaDecode(ConnectionString)
		if err2 != nil {
			log.Print("数据库连接解密失败：" + err2.Error())
			return ConnectionString
		}
		ConnectionString = str
	}
	return ConnectionString
}
