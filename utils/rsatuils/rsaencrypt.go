package rsatuils

import "github.com/wenzhenxi/gorsa"

// 公钥 加密用
var RsaPublickey = `-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDE+jESmUxyNj83Je0t0JC6Fovo
jnd4ME3Yhe244RYHukq0+ShX/+GB9PnAfw/k5Q4WlvBLZYA0BTKXSEbvvZBTLYaW
/fmC1cYpkdROdnfF/GikrvVCvgUIJ5LkXFR7pjQClDPEyIpihmD7noSTLoAXGL66
IdVnOEiW5J+bBtbMwwIDAQAB
-----END PUBLIC KEY-----
`

// 私钥 解密用
var RsaPrivatekey = `-----BEGIN PRIVATE KEY-----
MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAMT6MRKZTHI2Pzcl
7S3QkLoWi+iOd3gwTdiF7bjhFge6SrT5KFf/4YH0+cB/D+TlDhaW8EtlgDQFMpdI
Ru+9kFMthpb9+YLVximR1E52d8X8aKSu9UK+BQgnkuRcVHumNAKUM8TIimKGYPue
hJMugBcYvroh1Wc4SJbkn5sG1szDAgMBAAECgYEAqKAmcm2Z7AxUgbmOGbJ6s1Op
4iRUUifH/JxqreLDIUpxuguyZeNAAijdxdUAnksYjG4at/Dkl6P7c1cEoyf87Dcm
Yj6SoxSqNANS9j7oaSbbsare2uAk52zqDNXIrTL3tbcK9Y4keQvL0iIrgmffIvqp
UTPpRDVAVwLC9jQ52oECQQDq65IkaxUb05Zgg32v2qVFSTeEfWOHoO2c5iPZAm7Y
ju6YRKrHobDzgnQb85JkiuAzZaJLP/LtOS5LYn3moX3LAkEA1qcFtAt7hyWanTmg
6TmxDdVSV57w69oDK8uNl1X6kkR8f+2VfYXrI4LOHgJ41XxpMI/6GbPl04bK44O+
cGgN6QJAaGdFTdYhTXqdjEsj4rGjXcWKXOF1CGPm9iH5sIo6RRhQxW9PDuW3RKGn
hHydhP2LYow6soK7Ld6Jyor9NTB0PQJAOMzNp4z6TRLYTQ540/5eKajvNxxp6B8y
g55Dg25O9fGFeFyoGCUdybHNVQeWbfeT5jPTHIYMJOMgQQAVnq0KMQJAVpDBu/Pn
5NAvFtfbcNPMF0qrss2X68LI8P9c/kt4OTa4CuwuZwaWKzc5Ku3BsbTPwPGKO9aI
orMtC6KkVj5Cmw==
-----END PRIVATE KEY-----
`

// Rsa 公钥加密
func RsaEncode(endstr string) (result string, err error) {
	result, err = gorsa.PublicEncrypt(endstr, RsaPublickey)
	if err != nil {
		return
	}
	return
}

// Rsa 私解密
func RsaDecode(str string) (result string, err error) {

	result, err = gorsa.PriKeyDecrypt(str, RsaPrivatekey)
	if err != nil {
		return
	}

	if str != "" && result == "" {
		result = str
	}
	return
}
